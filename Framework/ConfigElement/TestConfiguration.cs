﻿using System.Configuration;

namespace Framework.ConfigElement
{
    public class FireTestConfiguration : ConfigurationSection
    {
        private static FireTestConfiguration _testConfig = (FireTestConfiguration)ConfigurationManager.GetSection("FireTestConfiguration");

        public static FireTestConfiguration Settings => _testConfig;

        [ConfigurationProperty("testSettings")]
        public FireFrameworkElementCollection TestSettings => (FireFrameworkElementCollection)base["testSettings"];
    }
}
