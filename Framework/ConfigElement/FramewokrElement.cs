﻿using System.Configuration;

namespace Framework.ConfigElement
{
    public class FireFrameworkElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name => (string)base["name"];

        [ConfigurationProperty("aut", IsRequired = true)]
        public string AUT => (string)base["aut"];

        [ConfigurationProperty("browser", IsRequired = true)]
        public string Browser => (string)base["browser"];

        [ConfigurationProperty("testType", IsRequired = false)]
        public string TestType => (string)base["testType"];

        [ConfigurationProperty("isFailConsoleErrors", IsRequired = false)]
        public string IsFailConsoleErrors => (string)base["isFailConsoleErrors"];

        [ConfigurationProperty("isCloseBrowser", IsRequired = false)]
        public string IsCloseBrowser => (string)base["isCloseBrowser"];

        [ConfigurationProperty("longTimeoutSeconds", IsRequired = false)]
        public string LongTimeoutSeconds => (string)base["longTimeoutSeconds"];

        [ConfigurationProperty("mediumTimeoutSeconds", IsRequired = false)]
        public string MediumTimeoutSeconds => (string)base["mediumTimeoutSeconds"];

        [ConfigurationProperty("shortTimeoutSeconds", IsRequired = false)]
        public string ShortTimeoutSeconds => (string)base["shortTimeoutSeconds"];

        [ConfigurationProperty("implicitlyWaitSeconds", IsRequired = false)]
        public string ImplicitlyWaitSeconds => (string)base["implicitlyWaitSeconds"];
    }
}
