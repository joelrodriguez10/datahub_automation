﻿using System.Configuration;

namespace Framework.ConfigElement
{
    [ConfigurationCollection(typeof(FireFrameworkElement), AddItemName = "testSetting", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class FireFrameworkElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new FireFrameworkElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as FireFrameworkElement).Name;
        }

        public new FireFrameworkElement this[string type] => (FireFrameworkElement)base.BaseGet(type);
    }
}
