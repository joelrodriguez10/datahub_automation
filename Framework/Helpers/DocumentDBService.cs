﻿using Microsoft.Azure.Documents.Client;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Framework.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Framework.Helpers
{
    public class DocumentDBService
    {
        private string endPoint;
        private string primaryKey;
        private DocumentClient client;

        public DocumentDBService(string endPointValue, string primaryKeyValue)
        {
            endPoint = endPointValue;
            primaryKey = primaryKeyValue;
            setClient().Wait();
        }

        public DocumentDBService()
        {
            endPoint = Settings.SqlEndPoint;
            primaryKey = Settings.SqlAccountKey;
        }

        public async Task setClient()
        {
            client = new DocumentClient(new Uri(endPoint), primaryKey);
            var list = await client.ReadDatabaseFeedAsync();
        }

        public dynamic createQuery(string collection, string database, string queryString)
        {
            try
            {
                var results = client.CreateDocumentQuery<Document>(UriFactory.CreateDocumentCollectionUri(collection, database), queryString).ToArray();

                return results;
            }
            catch(Exception)
            {
                Assert.Fail("Unable to query DocDB with supplied information. Collection: {0}, Database:{1}, Query:{2}", collection, database, queryString);
                return null;
            }
        }

        public IQueryable<Document> createIQuerable(string collection, string database, string queryString)
        {
            IQueryable<Document> results = client.CreateDocumentQuery<Document>(UriFactory.CreateDocumentCollectionUri(collection, database), queryString);
            var result = client.CreateDocumentQuery<Document>(UriFactory.CreateDocumentCollectionUri(collection, database), queryString).ToArray();

            return results;
        }
    }
}
