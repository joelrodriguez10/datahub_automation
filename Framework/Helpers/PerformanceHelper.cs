﻿using Framework.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace Framework.Helpers
{
    public class PerformanceHelper
    {
        /// <summary>
        /// The timer
        /// </summary>
        private readonly Stopwatch timer;

        /// <summary>
        /// The scenario list
        /// </summary>
        private readonly List<SavedTimes> loadTimeList;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceHelper"/> class.
        /// </summary>
        public PerformanceHelper()
        {
            loadTimeList = new List<SavedTimes>();
            timer = new Stopwatch();
        }

        /// <summary>
        /// Gets or sets the performance manager.
        /// </summary>
        /// <value>
        /// The performance manager.
        /// </value>
        public static PerformanceHelper Instance { get; set; }

        /// <summary>
        /// Gets the scenario list
        /// </summary>
        public IList<SavedTimes> GetloadTimeList => loadTimeList;

        /// <summary>
        /// Gets or sets measured time.
        /// </summary>
        /// <value>Return last measured time.</value>
        private double MeasuredTime { get; set; }

        /// <summary>
        /// Starts the measure.
        /// </summary>
        public void StartMeasure()
        {
            timer.Reset();
            timer.Start();
        }

        /// <summary>
        /// Stops the measure.
        /// </summary>
        /// <param name="title">The title.</param>
        public double StopMeasure(string title)
        {
            timer.Stop();

            var savedTimes = new SavedTimes(title);
            MeasuredTime = Math.Round(timer.Elapsed.TotalSeconds, 2);
            savedTimes.SetDuration(MeasuredTime);

            Console.WriteLine(title +" load time: {0}", MeasuredTime);

            loadTimeList.Add(savedTimes);
            return MeasuredTime;
        }

        /// <summary>
        /// Prints the performance summary.
        /// </summary>
        /// TODO: Decide what parameters must be printed
        public void PrintPerformanceResults(int limit = 0)
        {
            var groupedDurations = AllGroupedDurationsMilliseconds().Select(v =>
                v.StepName + " " + v.Browser + " Average: " + v.AverageDuration + "\n" +
                v.StepName + " " + v.Browser + " Percentile90Line: " + v.Percentile90);

            groupedDurations.ToList().ForEach(Console.WriteLine);

            if (limit != 0)
            {
                var average = Regex.Match(groupedDurations.ToList().FirstOrDefault(), @"\d+").Value;

                if (int.Parse(average) > limit)
                    throw new Exception("Performance time " + average + " secs is greater than " + limit + " secs!");
            }
        }

        /// <summary>
        /// All the durations milliseconds.
        /// </summary>
        /// <returns>Return average load times for particular scenarios and browsers.</returns>
        private IEnumerable<AverageGroupedTimes> AllGroupedDurationsMilliseconds()
        {
            var groupedList =
                loadTimeList.OrderBy(dur => dur.Duration).GroupBy(
                    st => new { st.Scenario, BName = st.BrowserName },
                    (key, g) =>
                    {
                        var savedTimeses = g as IList<SavedTimes> ?? g.ToList();
                        return new AverageGroupedTimes
                        {
                            StepName = key.Scenario,
                            Browser = key.BName,
                            AverageDuration = Math.Round(savedTimeses.Average(dur => dur.Duration)),
                            Percentile90 = savedTimeses[(int)(Math.Ceiling(savedTimeses.Count * 0.9) - 1)].Duration
                        };
                    }).ToList().OrderBy(listElement => listElement.StepName);
            return groupedList;
        }
    }
}
