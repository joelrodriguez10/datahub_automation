﻿using Framework.Base;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Tracing;

namespace Framework.Helpers
{
    /// <summary>
    /// Custom screenshot solution
    /// </summary>
    public static class TakeScreenShot
    {
        /// <summary>
        /// Gets or sets the test title.
        /// </summary>
        /// <value>
        /// The test title.
        /// </value>
        public static string TestTitle { get; set; }

        /// <summary>
        /// Gets or sets directory where assembly files are located
        /// </summary>
        public static string CurrentDirectory { get; set; }

        public static string FilePath { get; set; }

        /// <summary>
        /// Takes and saves screen shot
        /// </summary>
        public static bool TakeAndSaveScreenshot(string screenshootTitle = "")
        {
            try
            {
                string screenshotDir = CurrentDirectory;
                if (screenshootTitle != string.Empty)
                    TestTitle = screenshootTitle;

                if (string.IsNullOrEmpty(TestTitle)) TestTitle = "Screenshot";

                screenshotDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                screenshotDir = Path.Combine(screenshotDir, "ScreenShots");

                Directory.CreateDirectory(screenshotDir); // If the directory already exists, this method does nothing.

                var fileName = string.Format(CultureInfo.CurrentCulture, "{0}_{1}.png", TestTitle, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss", CultureInfo.CurrentCulture));
                if (fileName.Contains(" "))
                {
                    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                    fileName = textInfo.ToTitleCase(fileName);
                }

                fileName = Regex.Replace(fileName, "[^0-9a-zA-Z._]+", "");
                fileName = NameHelper.ShortenFileName(screenshotDir, fileName, "_", 255);
                FilePath = Path.Combine(screenshotDir, fileName);

                Bitmap ss = GetEntereScreenshot();
                if (ss == null)
                {
                    OpenQA.Selenium.Screenshot ss2 = ((ITakesScreenshot)DriverContext.Driver).GetScreenshot();
                    ss2.SaveAsFile(FilePath, ScreenshotImageFormat.Png);
                }
                else
                    ss.Save(FilePath, ImageFormat.Png);

                Console.WriteLine("Screen shot taken and saved to: " + FilePath);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error Taking Screen-shot!: " + e.Message);
                return false;
            }
        }

        private static Bitmap GetEntereScreenshot()
        {
            Bitmap stitchedImage = null;
            var js = (IJavaScriptExecutor)DriverContext.Driver;
            try
            {
                long totalwidth1 = (long)js.ExecuteScript("return document.body.offsetWidth");//documentElement.scrollWidth");

                long totalHeight1 = (long)js.ExecuteScript("return  document.body.parentNode.scrollHeight");

                int totalWidth = (int)totalwidth1;
                int totalHeight = (int)totalHeight1;

                // Get the Size of the View port
                long viewportWidth1 = (long)js.ExecuteScript("return document.body.clientWidth");//documentElement.scrollWidth");
                long viewportHeight1 = (long)js.ExecuteScript("return window.innerHeight");//documentElement.scrollWidth");

                int viewportWidth = (int)viewportWidth1;
                int viewportHeight = (int)viewportHeight1;


                // Split the Screen in multiple Rectangles
                List<Rectangle> rectangles = new List<Rectangle>();
                // Loop until the Total Height is reached
                for (int i = 0; i < totalHeight; i += viewportHeight)
                {
                    int newHeight = viewportHeight;
                    // Fix if the Height of the Element is too big
                    if (i + viewportHeight > totalHeight)
                    {
                        newHeight = totalHeight - i;
                    }
                    // Loop until the Total Width is reached
                    for (int ii = 0; ii < totalWidth; ii += viewportWidth)
                    {
                        int newWidth = viewportWidth;
                        // Fix if the Width of the Element is too big
                        if (ii + viewportWidth > totalWidth)
                        {
                            newWidth = totalWidth - ii;
                        }

                        // Create and add the Rectangle
                        Rectangle currRect = new Rectangle(ii, i, newWidth, newHeight);
                        rectangles.Add(currRect);
                    }
                }

                // Build the Image
                stitchedImage = new Bitmap(totalWidth, totalHeight);
                // Get all Screenshots and stitch them together
                Rectangle previous = Rectangle.Empty;
                foreach (var rectangle in rectangles)
                {
                    // Calculate the Scrolling (if needed)
                    if (previous != Rectangle.Empty)
                    {
                        int xDiff = rectangle.Right - previous.Right;
                        int yDiff = rectangle.Bottom - previous.Bottom;
                        // Scroll
                        //selenium.RunScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                        js.ExecuteScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                        System.Threading.Thread.Sleep(200);
                    }

                    // Take Screenshot
                    var screenshot = ((ITakesScreenshot)DriverContext.Driver).GetScreenshot();

                    // Build an Image out of the Screenshot
                    Image screenshotImage;
                    using (MemoryStream memStream = new MemoryStream(screenshot.AsByteArray))
                    {
                        screenshotImage = Image.FromStream(memStream);
                    }

                    // Calculate the Source Rectangle
                    Rectangle sourceRectangle = new Rectangle(viewportWidth - rectangle.Width, viewportHeight - rectangle.Height, rectangle.Width, rectangle.Height);

                    // Copy the Image
                    using (Graphics g = Graphics.FromImage(stitchedImage))
                    {
                        g.DrawImage(screenshotImage, rectangle, sourceRectangle, GraphicsUnit.Pixel);
                    }

                    // Set the Previous Rectangle
                    previous = rectangle;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while taking screenshot: {0}", ex);
            }
            return stitchedImage;
        }
    }

    /// <summary>
    /// This is an alternate screen capture method.
    /// </summary>
    public class Screenshot
    {
        public static void Capture(string fileNameBase = null)
        {
            try
            {
                string screenshotDir;

                fileNameBase = fileNameBase == null ? string.Format("error_{0}_{1}_{2}",
                                                    FeatureContext.Current.FeatureInfo.Title.ToIdentifier(),
                                                    ScenarioContext.Current.ScenarioInfo.Title.ToIdentifier(),
                                                    DateTime.Now.ToString("yyyyMMdd_HHmmss")) : fileNameBase;

                screenshotDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                screenshotDir = Path.Combine(screenshotDir, "Screenshots");

                Directory.CreateDirectory(screenshotDir); // If the directory already exists, this method does nothing.
                if (string.IsNullOrEmpty(fileNameBase)) fileNameBase = "Screenshot";

                ITakesScreenshot takesScreenshot = DriverContext.Driver as ITakesScreenshot;

                if (takesScreenshot != null)
                {
                    var screenshot = takesScreenshot.GetScreenshot();

                    string screenshotFilePath = Path.Combine(screenshotDir, fileNameBase + ".png");

                    screenshot.SaveAsFile(screenshotFilePath, ScreenshotImageFormat.Png);

                    Console.WriteLine("Screenshot: {0}", new Uri(screenshotFilePath));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while taking screenshot: {0}", ex);
            }
        }
    }
}

