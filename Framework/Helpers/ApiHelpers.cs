﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Framework.Helpers
{
    public class ApiHelpers
    {
        private static string _authorizationDailyApiKeyValue;
        private static string _authorizationTokenValue;
        private static string BaseUrl;
        private static DailyApiJson dailyApiJson;
        private static TokenJson tokenJson;

        public static string AuthorizationDailyApiKeyValue
        {
            get
            {
                return _authorizationDailyApiKeyValue;
            }
            private set { _authorizationDailyApiKeyValue = value; }
        }

        public static string AuthorizationTokenValue
        {
            get
            {
                if (!string.IsNullOrEmpty(_authorizationTokenValue))
                {
                    return _authorizationTokenValue;
                }
                else
                {
                    throw new Exception(message: "Authorization Token Value cannot be null.");
                }
            }
            private set { _authorizationTokenValue = value; }
        }

        /// <summary>
        /// Create Daily Key and store it on AuthorizationDailyApiKeyValue for later requests
        /// </summary>
        /// <param name="baseUrl"> Test Server Url</param>
        /// <param name="app_id"></param>
        /// <param name="app_key"></param>
        public static void SetDailyKey(string baseUrl, string app_id, string app_key, string dev_identifier = "")
        {
            if (string.IsNullOrEmpty(AuthorizationDailyApiKeyValue))
            {
                BaseUrl = baseUrl;
                var restClient = new RestClient(BaseUrl);
                var request = new RestRequest("api/v1/dailyapikey", Method.POST);
                request.RequestFormat = DataFormat.Json;

                SetDailyKeyObject(app_id, app_key, dev_identifier);
                var jsonRequest = request.JsonSerializer.Serialize(dailyApiJson);

                request.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);

                request.AddHeader("Content-type", "application/json; charset=utf-8");

                IRestResponse response = restClient.Execute(request);

                var jsonResponse = JsonConvert.DeserializeObject((dynamic)response.Content.ToString());

                if (response.ErrorMessage == null && response.StatusCode == System.Net.HttpStatusCode.OK)
                    AuthorizationDailyApiKeyValue = jsonResponse.get_Item("daily_api_key").ToString();
                else
                {
                    string error_message = String.Empty;

                    if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
                        error_message = response.StatusDescription;
                    else if (response.ErrorMessage != null)
                        error_message = response.ErrorMessage;
                    else if (jsonResponse != null)
                        error_message += " " + jsonResponse.get_Item("error_message").ToString();

                    Assert.Fail("Unable to get an daily api key \n Status code: {0} \n Error: {1} ",
                        response.StatusCode.ToString(),
                        error_message);
                }
            }
        }

        /// <summary>
        /// Create Token and store it on AuthorizationTokenValue for later requests 
        /// </summary>
        /// <param name="data"> Retrieved from a Json file</param>
        /// <param name="expiration_minutes"></param>
        /// <param name="process_name"></param>
        public static void CreateJWTToken(Dictionary<string, dynamic> data, int expiration_minutes, string process_name)
        {
            List<Claim> claims = new List<Claim>();
            var restClient = new RestClient(BaseUrl);
            var request = new RestRequest("api/v1/createtoken", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-type", "application/json; charset=utf-8");

            //add the key as a parameter
            request.AddQueryParameter("api_key", AuthorizationDailyApiKeyValue);
            SetTokenObject(data, claims, expiration_minutes, process_name);

            var jsonRequest = request.JsonSerializer.Serialize(tokenJson);

            request.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);

            //send request
            IRestResponse response = restClient.Execute(request);

            var jsonResponse = JsonConvert.DeserializeObject((dynamic)response.Content.ToString());

            if (response.ErrorMessage == null && response.StatusCode == System.Net.HttpStatusCode.OK)
                AuthorizationTokenValue = jsonResponse.get_Item("token").ToString();
            else
            {
                string error_message = String.Empty;

                if (response.ErrorMessage != null)
                    error_message = response.ErrorMessage;

                if (jsonResponse.get_Item("daily_api_key") != null)
                    error_message += " " + jsonResponse.get_Item("error_message").ToString();

                Assert.Fail("JWT Daily Token Request Received A Bad Response \n Status code: {0} \n Error: {1} ",
                    response.StatusCode.ToString(),
                    error_message);
            }
        }

        class DailyApiJson
        {
            public string app_id { get; set; }
            public string app_key { get; set; }
            public string dev_identifier { get; set; }
        }

        class Claim
        {
            public string type { get; set; }
            public string value { get; set; }
        }

        class TokenJson
        {
            public List<Claim> claims { get; set; }
            public int expiration_minutes { get; set; }
            public string process_name { get; set; }
        }

        static void SetDailyKeyObject(string app_id_value, string app_key_value, string dev_identifier_value = "")
        {
            dailyApiJson = new DailyApiJson
            {
                app_id = app_id_value,
                app_key = app_key_value,
                dev_identifier = dev_identifier_value
            };
        }

        static void SetTokenObject(Dictionary<string, dynamic> data, List<Claim> claims, int expiration_minutes_value, string process_name_value)
        {
            tokenJson = new TokenJson
            {
                expiration_minutes = expiration_minutes_value,
                process_name = process_name_value,
                claims = new List<Claim>()
            };

            //add info to json dynamically
            foreach (KeyValuePair<string, dynamic> pair in data)
            {
                tokenJson.claims.Add(new Claim { type = pair.Key, value = pair.Value });
            }
        }
    }
}
