﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Framework.Helpers
{
    /// <summary>
    /// Contains useful actions connected with test data
    /// </summary>
    public static class NameHelper
    {
        /// <summary>
        /// Create random name.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns>Random name</returns>
        public static string RandomName(int length)
        {
            const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var randomString = new StringBuilder();
            var random = new Random();

            for (int i = 0; i < length; i++)
            {
                randomString.Append(Chars[random.Next(Chars.Length)]);
            }

            return randomString.ToString();
        }

        /// <summary>
        /// Shortens the file name by removing occurrences of given pattern till length of folder + filename will be shorten than max Length.
        /// </summary>
        /// <param name="folder">The folder.</param>
        /// <param name="fileName">The fileName.</param>
        /// <param name="pattern">The regular expression pattern to match</param>
        /// <param name="maxLength">Max length</param>
        /// <returns>String with removed all patterns</returns>
        /// <example>How to use it: <code>
        /// NameHelper.ShortenFileName(folder, correctFileName, "_", 255);
        /// </code></example>
        public static string ShortenFileName(string folder, string fileName, string pattern, int maxLength)
        {
            while (((folder + fileName).Length > maxLength) && fileName.Contains(pattern))
            {
                Regex rgx = new Regex(pattern);
                fileName = rgx.Replace(fileName, string.Empty, 1);
            }

            if ((folder + fileName).Length > 255)
            {
                Console.Write(string.Format("Length of the file full name is over {0} characters, try to shorten the name of tests", maxLength));
            }

            return fileName;
        }
    }
}
