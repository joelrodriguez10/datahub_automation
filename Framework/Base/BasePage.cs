﻿using System.Text.RegularExpressions;

namespace Framework.Base
{
    public abstract class BasePage : Base
    {       
        protected string removeSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }
    }
}