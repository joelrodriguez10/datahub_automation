﻿using System;
using TechTalk.SpecFlow;

namespace Framework.Base
{
    public class Base
    {
        public BasePage CurrentPage
        {
            get
            {
                try
                {
                    return (BasePage)ScenarioContext.Current["currentPage"];
                }
                catch (System.Exception)
                {

                    throw new InvalidOperationException("You need to assign CurrentPage (i.e. CurrentPage = GetInstance<SearchPage>();");
                }
            }
            set
            {
                ScenarioContext.Current["currentPage"] = value;
            }
        }

        protected TPage GetInstance<TPage>() where TPage : BasePage, new()
        {
            return (TPage)Activator.CreateInstance(typeof(TPage));
        }

        public TPage As<TPage>() where TPage : BasePage
        {
            return (TPage)this;
        }
    }
}
