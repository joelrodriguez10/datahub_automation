﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashFlow.Steps
{
	public class Login
	{
		[Given(@"I navigate to the application")]
		public void GivenINavigateToTheApplication()
		{
			ScenarioContext.Current.Pending();
		}

		[When(@"I enter valid Credentials for ""(.*)""")]
		public void WhenIEnterValidCredentialsFor(string p0)
		{
			ScenarioContext.Current.Pending();
		}

		[Then(@"I should be successfully logged in")]
		public void ThenIShouldBeSuccessfullyLoggedIn()
		{
			ScenarioContext.Current.Pending();
		}

	}
}
