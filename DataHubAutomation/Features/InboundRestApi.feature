﻿Feature: InboundRestApi

@API @Domain @Get @Inbound
Scenario Outline: Inbound GET Domain files Information
Given I have a GET request for "<ClientId>" for Inbound Api
When I execute the Get request
And I should get a successful response
Then I should get back results

Examples: 
| Scenario | ClientId     |
| Client 1 | DomainClient |

@API @Domain @Get @Inbound
Scenario Outline: Inbound GET SubDomain files Information
Given I have a GET request for "<ClientId>" for Inbound Api
When I execute the Get request
And I should get a successful response
Then I should get back results

Examples: 
| Scenario | ClientId        |
| Client 2 | SubDomainClient |

@API @Domain @Get @Inbound
Scenario Outline: Inbound GET Domain Directory SubDomain file information 
Given I have a GET request for "<ClientId>" for Directory Inbound Api
When I execute the Get request
And I should get a successful response
Then I should get back results

Examples: 
| Scenario | ClientId     |
| Client 1 | DomainClient |

@API @Domain @Post @Inbound
Scenario Outline: Inbound POST Upload Domain Files
Given I have a "<file>" to upload for "<Workflow>"
And I have a POST request for "<ClientId>" for Inbound Api
When I execute the Post request to upload file
Then I should get back successful response

Examples: 
| Scenario | ClientId     | Workflow          | file |
| Client 1 | DomainClient | WorkFlowTransform | .csv |

@API @Domain @Post @Inbound
Scenario Outline: Inbound POST Upload Subdomain Files
Given I have a "<file>" to upload for "<Workflow>"
And I have a POST request for "<ClientId>" for Inbound Api
When I execute the Post request to upload file
Then I should get back successful response

Examples: 
| Scenario | ClientId        | Workflow          | file |
| Client 2 | SubDomainClient | WorkFlowTransform | .csv |