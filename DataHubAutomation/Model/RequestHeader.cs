﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataHubAutomation.Model
{
	public class RequestHeader
	{
		public string id { get; set; }
		public string clientId { get; set; }
		public string product { get; set; }
		public string domain { get; set; }
		public string x_api_key {get; set;}
	}
}
