﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using DataHubAutomation.Model;
using Framework.Base;
using Framework.Config;
using Framework.Helpers;
using DataHubAutomation.Helper;
using RestSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using Framework.ConfigElement;
using DataHubAutomation.Pages;
using Newtonsoft.Json;
using Dynamitey.DynamicObjects;
using System.Web.UI.WebControls;
using System.IO;
using Newtonsoft.Json.Linq;

namespace DataHubAutomation.Steps
{

	[Binding]

	public class InOutboundSteps : BaseStep
	{
		private Request request;
		private IRestResponse response;

		[Given(@"I have a GET request for ""(.*)"" for Outbound Api")]
		public void GivenIHaveAGETRequestForForOutboundApi(string client)
		{
			CurrentPage = GetInstance<InOutboundPage>();
			CurrentPage.As<InOutboundPage>().GetRequestOutboundDomain(client);
		}

		[Given(@"I have a GET request for ""(.*)"" for Directory Outbound Api")]
		public void GivenIHaveAGETRequestForForDirectoryOutboundApi(string client)
		{
			CurrentPage = GetInstance<InOutboundPage>();
			string directory = "/dir";
			CurrentPage.As<InOutboundPage>().GetRequestOutboundDomain(client);
			ScenarioContext.Current["method"] = ScenarioContext.Current.Get("method") + directory;
		}


		[When(@"I execute the Get request")]
		public void WhenIExecuteTheGetRequest()
		{
			CurrentPage = GetInstance<InOutboundPage>();
			var baseUrl = FireTestConfiguration.Settings.TestSettings[ScenarioContext.Current.Get("type")].AUT;
			var method = ScenarioContext.Current.Get("method");
			this.response = RestHelper.GetResponse(baseUrl, method);
		}

		[When(@"I should get a successful response")]
		public void WhenIShouldGetASuccessfulResponse()
		{
			CurrentPage = GetInstance<InOutboundPage>();
			Assert.IsTrue(this.response.IsSuccessful == true, "Not A Successfule Response!!!!");
		}

		[Then(@"I should get back results")]
		public void ThenIShouldGetBackResults()
		{
			CurrentPage = GetInstance<InOutboundPage>();
			string jsonFormatted = JValue.Parse(this.response.Content).ToString(Formatting.Indented);
			Console.WriteLine(jsonFormatted);
			var content = JsonConvert.DeserializeObject<List<Response>>(this.response.Content.ToString());
			Assert.IsTrue(content.Count > 0, "No Results Returned!!!!");
		}

		[Given(@"I have a GET request for ""(.*)"" for download Outbound Api")]
		public void GivenIHaveAGETRequestForForDownloadOutboundApi(string client)
		{
			CurrentPage = GetInstance<InOutboundPage>();
			CurrentPage.As<InOutboundPage>().GetRequestDownloadOutbound(client);
		}

		[Then(@"I should have a downloaded file")]
		public void ThenIShouldHaveADownloadedFile()
		{
			CurrentPage = GetInstance<InOutboundPage>();
			var filepath = Path.Combine(Environment.CurrentDirectory, @"Downloads\", ScenarioContext.Current.Get("FileName"));
			var file = RestHelper.DownloadedFile(filepath);
			Assert.IsTrue(File.Exists(filepath), "File does not exist");
			Console.WriteLine(File.Exists(filepath) ? "File Exists!!!" : "File doesn't exist!!!");
		}

		[Given(@"I need to select file id from ""(.*)""")]
		public void GivenINeedToSelectFileIdFrom(string client)
		{
			CurrentPage = GetInstance<InOutboundPage>();
			CurrentPage.As<InOutboundPage>().GetRequestOutboundDomain(client);
			WhenIExecuteTheGetRequest();
			CurrentPage.As<InOutboundPage>().RandomFileId(response);
		}

		[Given(@"I have a GET request for ""(.*)"" for Inbound Api")]
		public void GivenIHaveAGETRequestForForInboundApi(string client)
		{
			CurrentPage = GetInstance<InOutboundPage>();
			CurrentPage.As<InOutboundPage>().GetRequestInboundDomain(client);
		}

		[Given(@"I have a GET request for ""(.*)"" for Directory Inbound Api")]
		public void GivenIHaveAGETRequestForForDirectoryInboundApi(string client)
		{
			CurrentPage = GetInstance<InOutboundPage>();
			string directory = "/dir";
			CurrentPage.As<InOutboundPage>().GetRequestInboundDomain(client);
			ScenarioContext.Current["method"] = ScenarioContext.Current.Get("method") + directory;
		}


		[Given(@"I have a ""(.*)"" to upload for ""(.*)""")]
		public void GivenIHaveAToUploadFor(string fileType, string workflow)
		{
			CurrentPage = GetInstance<InOutboundPage>();
			TestHelpers.CreateFile(fileType, workflow);
		}

		[Given(@"I have a POST request for ""(.*)"" for Inbound Api")]
		public void GivenIHaveAPOSTRequestForForInboundApi(string client)
		{
			CurrentPage = GetInstance<InOutboundPage>();
			CurrentPage.As<InOutboundPage>().GetRequestInboundDomain(client);
		}

		[When(@"I execute the Post request to upload file")]
		public void WhenIExecuteThePostRequestToUploadFile()
		{

			CurrentPage = GetInstance<InOutboundPage>();
			var method = ScenarioContext.Current.Get("method");
			var baseUrl = FireTestConfiguration.Settings.TestSettings[ScenarioContext.Current.Get("type")].AUT;
			this.response = RestHelper.PostDownloadResponse(baseUrl, method);
		}

		[Then(@"I should get back successful response")]
		public void ThenIShouldGetBackSuccessfulResponse()
		{
			CurrentPage = GetInstance<InOutboundPage>();
			Assert.IsTrue(this.response.IsSuccessful == true, "Response Failed!!!!!!!");
		}
	}
}
