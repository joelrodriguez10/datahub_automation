﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using TechTalk.SpecFlow;
using DataHubAutomation.Model;
using Framework.Base;
using Framework.Config;
using Framework.Helpers;
using DataHubAutomation.Helper;
using Dynamitey.DynamicObjects;
using System.Net.Http;
using System.IO;
using System.Web;
using Framework.ConfigElement;

namespace DataHubAutomation.Helper
{
	public class RestHelper
	{
		private static string BaseUrl;

		public static Response PostResponse(string baseUrl, string method, Request request)
		{
			BaseUrl = baseUrl;
			var restClient = new RestClient(BaseUrl);
			var restRequest = new RestRequest(method, Method.POST);
			restRequest.RequestFormat = DataFormat.Json;
			var jsonRequest = restRequest.JsonSerializer.Serialize(request);
			restRequest.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);
			restRequest.AddHeader("X-API-Key", ScenarioContext.Current.Get("X-Api-Key"));
			restRequest.AddHeader("Content-type", "application/json");
			IRestResponse restResponse = restClient.Execute(restRequest);
			var resp = JsonConvert.DeserializeObject<Response>(restResponse.Content.ToString());

			return resp;
		}

		public static IRestResponse PostDownloadResponse(string baseUrl,string method)
		{
			BaseUrl = baseUrl;
			string name = ScenarioContext.Current.Get("FileName");
			string fullPath = ScenarioContext.Current.Get("FullFilePath");
			var restClient = new RestClient(BaseUrl);
			var restRequest = new RestRequest(method, Method.POST);
			restRequest.AddHeader("X-API-Key", ScenarioContext.Current.Get("X-Api-Key"));
			restRequest.AddHeader("Accept", "application/json");
			restRequest.RequestFormat = DataFormat.Json;
			restRequest.AlwaysMultipartFormData = true;
			restRequest.AddFile(name, fullPath, "multipart/form-data");
			var restResponse = restClient.Execute(restRequest);
			return restResponse;
		}

		public static IRestResponse GetResponse(string baseUrl, string method)
		{
			var restClient = new RestClient(baseUrl);
			var restRequest = new RestRequest(method, Method.GET);
			restRequest.AddHeader("Accept", "application/json");
			restRequest.AddHeader("X-API-Key", ScenarioContext.Current.Get("X-Api-Key"));
			restRequest.RequestFormat = DataFormat.Json;
			return restClient.Execute(restRequest);
		}

		public static IRestResponse DownloadedFile(string filePath)
		{
			
			var method = ScenarioContext.Current.Get("method");
			var baseUrl = FireTestConfiguration.Settings.TestSettings[ScenarioContext.Current.Get("type")].AUT;
			var client = new RestClient(baseUrl);
			var restRequest = new RestRequest(method, Method.GET);
			restRequest.AddHeader("Accept", "application/json");
			restRequest.AddHeader("X-API-Key", ScenarioContext.Current.Get("X-Api-Key"));
			restRequest.RequestFormat = DataFormat.Json;
			byte[] response = client.DownloadData(restRequest);
			File.WriteAllBytes(filePath, response);
			return client.Execute(restRequest);
		} 
	}
}
