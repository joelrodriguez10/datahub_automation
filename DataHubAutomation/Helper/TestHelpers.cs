﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using TechTalk.SpecFlow;
using System.IO;
using System.Web;
using Framework.ConfigElement;
using Framework.Helpers;

namespace DataHubAutomation.Helper
{
	public class TestHelpers
	{

		public static void CreateFile(string type, string workflow)
		{
			string fileName = DateTime.Now.ToString("MM_dd_yyyy_HHmm") + "_" + workflow + type;
			string fileNameType = workflow + type;
			string fileLocation = Path.Combine(Environment.CurrentDirectory, @"Downloads\");
			string fullFilePath = Path.Combine(fileLocation, fileName);
			ScenarioContext.Current["FullFilePath"] = fullFilePath;
			ScenarioContext.Current["FileName"] = fileName;
			ScenarioContext.Current["FilePath"] = fileLocation;
			try
			{
				using (StreamReader reader = new StreamReader(File.OpenRead(Path.Combine(Environment.CurrentDirectory, @"TextFiles", workflow + ".txt"))))
				{
					using (StreamWriter writer = new StreamWriter(File.Open(fullFilePath, FileMode.Create)))
					{
						string line;
						while ((line = reader.ReadLine()) != null)
						{
							writer.WriteLine(line);
						}
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}

			
				
		}
	}
}
